package tabulky;
// Generated May 4, 2013 3:13:50 PM by Hibernate Tools 3.2.1.GA



/**
 * JeVPosadke generated by hbm2java
 */
public class JeVPosadke  implements java.io.Serializable {


     public Integer idJeVPosadkePk;
     public Let let;
     public Zamestnanec zamestnanec;
     public PoziciaVPosadke poziciaVPosadke;

    public JeVPosadke() {
    }

    public JeVPosadke(Let let, Zamestnanec zamestnanec, PoziciaVPosadke poziciaVPosadke) {
       this.let = let;
       this.zamestnanec = zamestnanec;
       this.poziciaVPosadke = poziciaVPosadke;
    }
   
    public Integer getIdJeVPosadkePk() {
        return this.idJeVPosadkePk;
    }
    
    public void setIdJeVPosadkePk(Integer idJeVPosadkePk) {
        this.idJeVPosadkePk = idJeVPosadkePk;
    }
    public Let getLet() {
        return this.let;
    }
    
    public void setLet(Let let) {
        this.let = let;
    }
    public Zamestnanec getZamestnanec() {
        return this.zamestnanec;
    }
    
    public void setZamestnanec(Zamestnanec zamestnanec) {
        this.zamestnanec = zamestnanec;
    }
    public PoziciaVPosadke getPoziciaVPosadke() {
        return this.poziciaVPosadke;
    }
    
    public void setPoziciaVPosadke(PoziciaVPosadke poziciaVPosadke) {
        this.poziciaVPosadke = poziciaVPosadke;
    }




}


