package tabulky;
// Generated May 4, 2013 3:13:50 PM by Hibernate Tools 3.2.1.GA



/**
 * Zahrnuje generated by hbm2java
 */
public class Zahrnuje  implements java.io.Serializable {


     public Integer idZahrnujePk;
     public Linka linka;
     public Letisko letisko;
     public Integer poradie;

    public Zahrnuje() {
    }

    public Zahrnuje(Linka linka, Letisko letisko, Integer poradie) {
       this.linka = linka;
       this.letisko = letisko;
       this.poradie = poradie;
    }
   
    public Integer getIdZahrnujePk() {
        return this.idZahrnujePk;
    }
    
    public void setIdZahrnujePk(Integer idZahrnujePk) {
        this.idZahrnujePk = idZahrnujePk;
    }
    public Linka getLinka() {
        return this.linka;
    }
    
    public void setLinka(Linka linka) {
        this.linka = linka;
    }
    public Letisko getLetisko() {
        return this.letisko;
    }
    
    public void setLetisko(Letisko letisko) {
        this.letisko = letisko;
    }
    public Integer getPoradie() {
        return this.poradie;
    }
    
    public void setPoradie(Integer poradie) {
        this.poradie = poradie;
    }




}


