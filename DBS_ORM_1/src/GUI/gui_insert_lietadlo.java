/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Frame;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javaapplication4.hlavne;
import javaapplication4.poziadavka;
import javaapplication4.tabulka_stlpce;
import javax.swing.JOptionPane;

/**
 *
 * @author aaa
 */
public class gui_insert_lietadlo extends javax.swing.JFrame {

    /**
     * Creates new form gui_insert_lietadlo
     */
    String tabulka = "Lietadlo";
    int poc1 = -1;
    poziadavka poz;
    ArrayList<String> stlpce;
        ArrayList<String> hodnoty; 
        tabulka_stlpce vloz;        
        ArrayList<tabulka_stlpce> vsetko;
        String hladam_tabulku;
        String hladam_nazov_atributu;
        String hladam_hodnotu_atributu;
        int kolky_atribut;
        int vratim_pk;
        int pom_pk;
        int kolky_join = 0;
    
    
    
    public gui_insert_lietadlo() {
        tabulka = "Lietadlo";
        poc1 = -1;
        
        stlpce = new ArrayList<String>();
        hodnoty = new ArrayList<String>(); 
        vloz = new tabulka_stlpce();        
        vsetko = new ArrayList<>();
        poz = new poziadavka();
        hladam_tabulku = "";
        hladam_nazov_atributu = "";
        hladam_hodnotu_atributu = "";
        kolky_atribut = -1;
        vratim_pk = -1;
        pom_pk = -1;
        kolky_join = 0;
        
        
        initComponents();
        druha_inicializacia();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel12 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTextField10 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        jLabel12.setText("jLabel12");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Hodnota");

        jTextField1.setText("0");

        jLabel2.setText("Cena za prenajom");

        jTextField2.setText("0");

        jLabel3.setText("Stav (slovne)");

        jLabel4.setText("Datum vyroby (YYYY-MM-DD)");

        jLabel5.setText("Datum zaradenia do sluzby (YYYY-MM-DD)");

        jLabel6.setText("Zivotnost (YYYY-MM-DD)");

        jLabel7.setText("Dalsie informacie o lietadle");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jLabel8.setText("Vlastnik");

        jLabel9.setText("Typ lietadla");

        jLabel10.setText("Letisko, na ktorom sa prave nachadza");

        jButton1.setText("Spat");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Insert");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });

        jLabel11.setText("ID");

        jButton3.setText("Delete");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Update");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel13.setText("Zoskupiť podľa");

        jButton5.setText("Select (aspoň 1)");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("Select (všetky)");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextField6)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jTextField5)
                                    .addComponent(jTextField4)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextField3, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jTextField2)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addComponent(jLabel6)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(jLabel7)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jTextField9)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField7, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jTextField8, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11)
                                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addGap(25, 25, 25))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton6)
                    .addComponent(jButton5))
                .addGap(27, 27, 27))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7))
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel6)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jScrollPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(35, 35, 35)
                .addComponent(jButton5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.setVisible(false);
        hlavne.gui_i.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        poz.vynuluj();
        poz.setTyp_poziadavky("INSERT");
        poz.setUlozit(true);
        vykonaj(poz);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        poz.vynuluj();
        poz.setTyp_poziadavky("DELETE");
        vykonaj(poz);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        poz.vynuluj();
        poz.setTyp_poziadavky("UPDATE");
        poz.setUlozit(true);
        vykonaj(poz);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        poz.vynuluj();
        poz.setTyp_poziadavky("SELECT");
        poz.setUlozit(true);
        poz.setOp("OR");
        vykonaj(poz);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        poz.vynuluj();
        poz.setTyp_poziadavky("SELECT");
        poz.setUlozit(true);
        poz.setOp("AND");
        vykonaj(poz);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(gui_insert_lietadlo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(gui_insert_lietadlo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(gui_insert_lietadlo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(gui_insert_lietadlo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new gui_insert_lietadlo().setVisible(true);
            }
        });
    }
    
   public void vykonaj(poziadavka p)
    {
        
        hodnoty.clear();
        stlpce.clear();
        kolky_join = 0;
        
        
         stlpce.add("idLietadloPk");
        if(jTextField10.getText().equals(""))
        {
            hodnoty.add(null);
        }
        else
        {           
            hodnoty.add(jTextField10.getText());
            poz.setPk(Integer.parseInt(jTextField10.getText()));
        }
        
        stlpce.add("hodnota");
        if(jTextField1.getText().equals(""))
        {
           hodnoty.add(null);            
        }
        else
        {
            hodnoty.add(jTextField1.getText());
        }
        
        stlpce.add("cenaZaPrenajom");
        if(jTextField2.getText().equals(""))
        {
            hodnoty.add(null);
        }
        else
        {            
            hodnoty.add(jTextField2.getText());
        }
        
        stlpce.add("stav");
        if(jTextField3.getText().equals(""))
        {
            hodnoty.add(null);
        }
        else
        {           
            hodnoty.add(jTextField3.getText());
        }
        stlpce.add("datumVyroby");
        if(jTextField4.getText().equals(""))
        {
            hodnoty.add(null);
        }
        else
        {           
            hodnoty.add(jTextField4.getText());
        }
        
        stlpce.add("datumZaradeniaDoSluzby");
        if(jTextField5.getText().equals(""))
        {
            hodnoty.add(null);
        }
        else
        {           
            hodnoty.add(jTextField5.getText());
        }
        
        stlpce.add("zivotnost");
        if(jTextField6.getText().equals(""))
        {
            hodnoty.add(null);
        }
        else
        {           
            hodnoty.add(jTextField6.getText());
        }
        
       
        
        stlpce.add("poznamkaLietadlo");
        if(jTextArea1.getText().equals(""))
        {
            hodnoty.add(null);
        }
        else
        {           
            hodnoty.add(jTextArea1.getText());
        }
//        for(String hovno : hodnoty)
//            System.out.println("PRECO?: "+hovno);
        
       
//       stlpce.add("majitel");
//       if (nastav_tunajsi_foreign_key(jTextField7.getText(), "nazovMajitela")!=0) return;
//       stlpce.add("typLietadla");
//       if (nastav_tunajsi_foreign_key(jTextField8.getText(), "typL")!=0) return;
//       stlpce.add("letisko");
//       if (nastav_tunajsi_foreign_key(jTextField9.getText(), "nazov")!=0) return;
        
   //     stlpce.add("majitel");
        nastav_join("nazovMajitela",jTextField7.getText(), "Majitel");
   //     stlpce.add("typLietadla");
        nastav_join("typL",jTextField8.getText(), "TypLietadla");
   //     stlpce.add("letisko");
        nastav_join("nazov",jTextField9.getText(), "Letisko");
       
       
       if(jComboBox1.getSelectedItem().equals(""))
        {
            poz.setGroup_by(null);
        }
        else
        {
            poz.setGroup_by((String)jComboBox1.getSelectedItem());
        }
        
//        if(jCheckBox1.isSelected())
//        {
//            poz.setCela_tabulka(true);
//        }
       
       
    //   System.out.println("KDE TO MA PRIDAT?");
       
       vloz.setStlpce(stlpce);
       vloz.setHodnoty(hodnoty);
       vloz.setTabulka(tabulka);
       vsetko.add(vloz);
       p.setTabulky(vsetko);
       if(p.getTyp_poziadavky().equals("INSERT"))
       {
           System.out.println("Insertujem lietadlo");
           hlavne.vt.insert(p);
       }
       if(p.getTyp_poziadavky().equals("SELECT"))
           hlavne.vt.select(p);
       if(p.getTyp_poziadavky().equals("DELETE"))
            hlavne.vt.delete(p);
       if(p.getTyp_poziadavky().equals("UPDATE"))
           hlavne.vt.update(p);
    }
    
    
   public int nastav_join(String t, String u, String v)
   {
       if(u.equals(""))
       {
        //   hodnoty.add(null);
           return 0;
       }
       poz.getJoin().add(v);    //tabulka
       poz.getOn_1().add(t);    //nazov atributu
       poz.getOn_2().add(u);    //jeho hodnota
       //poz.getJoin2().add
       
       return 0;
   }
   
   
   public int nastav_tunajsi_foreign_key(String t, String u)
   {
       hladam_nazov_atributu = u;
       hladam_hodnotu_atributu = t;
       hladam_tabulku = "tabulky."+stlpce.get(stlpce.size()-1).substring(0,stlpce.get(stlpce.size()-1).length() - 3);
       if(!t.equals(""))
       {
           poz.getJoin().add(new String());
           poz.getOn_1().add(new String());
           poz.getOn_2().add(new String());
           poz.getJoin().set(kolky_join, hladam_tabulku.substring(hladam_tabulku.lastIndexOf(".")+1));
           poz.getOn_1().set(kolky_join, stlpce.get(stlpce.size()-1));
           poz.getOn_2().set(kolky_join, "id_"+poz.getOn_1().get(kolky_join));
           poz.getOn_2().set(kolky_join, poz.getOn_2().get(kolky_join).substring(0, poz.getOn_2().get(kolky_join).length()-3) + "_pk");

           poz.getOn_1().set(kolky_join, tabulka+"."+poz.getOn_1().get(kolky_join));
           poz.getOn_2().set(kolky_join, poz.getJoin().get(kolky_join)+"."+poz.getOn_2().get(kolky_join));
           kolky_join++;
       }
       poc1= -1;
       if(hladam_hodnotu_atributu.equals(""))
       {
           hodnoty.add(null);
           kolky_atribut = -1;
           pom_pk = -1;
           vratim_pk = -1;
           hladam_tabulku = "";
           hladam_nazov_atributu = "";
           hladam_hodnotu_atributu = "";
           return 0;
       }
       for(String s : hlavne.mena_listov)
       {
           poc1++;
           if(hladam_tabulku.equals(s))
               break;           
       }
       if(poc1 == -1)
       {
           JOptionPane.showMessageDialog(new Frame(),"CHYBA: Tabulka "+hladam_tabulku+" neexistuje, ale mala by, alebo nevhodne nazvany stlpec vo formulari");
           kolky_atribut = -1;
           pom_pk = -1;
           vratim_pk = -1;
           hladam_tabulku = "";
           hladam_nazov_atributu = "";
           hladam_hodnotu_atributu = "";
           return 2;
       }
       
 //      System.out.println(poc1+" +++++"+hlavne.mena_listov.get(poc1)+" a co kurva chcem? "+hladam_tabulku+" hladam_hodnotu: "+hladam_hodnotu_atributu);
       for(Object ob : hlavne.listy.get(poc1))
       {
           try
           {
               pom_pk = ob.getClass().getFields()[0].getInt(ob);
               if(kolky_atribut== -1)
                   for(Field atribut : ob.getClass().getFields())
                   {
                       kolky_atribut++;
                       if(atribut.getName().equals(hladam_nazov_atributu))
                       {   
                           break;
                       }
                   }
               else
               {
                   try
                   {
                       if(ob.getClass().getFields()[kolky_atribut].get(ob).toString().equals(hladam_hodnotu_atributu))
                       {
                           vratim_pk = pom_pk;
                           hodnoty.add(Integer.toString(vratim_pk));
                           break;
                       }
                   }
                   catch(NullPointerException e)
                   {
                       
                   }
               }
              
           }
           catch(Exception e)
           {
               System.out.println("CHYBA");
               e.printStackTrace();
           }
           
       }
       if(vratim_pk == -1)
       {
           JOptionPane.showMessageDialog(new Frame(),"CHYBA: neexistujuci "+hladam_nazov_atributu);
           kolky_atribut = -1;
           pom_pk = -1;
           vratim_pk = -1;
           hladam_tabulku = "";
           hladam_nazov_atributu = "";
           hladam_hodnotu_atributu = "";
           return 3;
       }
       System.out.println("hodnota atributu: "+hladam_hodnotu_atributu+ " .. kolky_atribut"+kolky_atribut);
       kolky_atribut = -1;
       pom_pk = -1;
       vratim_pk = -1;
       hladam_tabulku = "";
       hladam_nazov_atributu = "";
       hladam_hodnotu_atributu = "";
       return 0;
   }
   
   
   public void druha_inicializacia()
     {
         Object o = new Object();
         Class c;
         ArrayList<String> moznosti = new ArrayList<>();
         
         try
         {
             c = Class.forName("tabulky."+tabulka);
             o = c.newInstance();
         }
         catch(Exception e)
         {
             e.printStackTrace();
         }
         moznosti.add("");
         for(Field f : o.getClass().getFields())
         {
             moznosti.add(f.getName());
         }
         
         
     //    jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Vypis po 8", "Vypis po  16", "Vypis po 32" }));
         jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(moznosti.toArray()));
        jComboBox1.setSelectedIndex(0);
        jComboBox1.setToolTipText("");
        jComboBox1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                //jComboBox1ActionPerformed(evt);
            }
        });
     }
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
