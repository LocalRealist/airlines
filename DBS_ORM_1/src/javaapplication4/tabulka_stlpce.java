/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

import java.util.ArrayList;

/**
 *
 * @author aaa
 */
public class tabulka_stlpce 
{
    public ArrayList<String> hodnoty = new ArrayList<>();
    public ArrayList<String> stlpce = new ArrayList<>();
    public String tabulka;

    
    public tabulka_stlpce(ArrayList<String> a, ArrayList<String> b, String c)
    {
        hodnoty = a;
        stlpce = b;
        tabulka = c;
    }

    public tabulka_stlpce()
    {
        
    }
    
    
    
   public void vynuluj()
    {
        hodnoty = stlpce = null;
        tabulka = null;
    }
   
   
   public boolean je_nejaka_hodnota()
    {
        for(String s : hodnoty)
        {
            if(!(s==null))
                return true;
        }
        return false;
    }
   

    /**
     * @return the tabulka
     */
    public String getTabulka() {
        return tabulka;
    }

    /**
     * @param tabulka the tabulka to set
     */
    public void setTabulka(String tabulka) {
        this.tabulka = tabulka;
    }

    /**
     * @return the hodnoty
     */
    public ArrayList<String> getHodnoty() {
        return hodnoty;
    }

    /**
     * @param hodnoty the hodnoty to set
     */
    public void setHodnoty(ArrayList<String> hodnoty) {
        this.hodnoty = hodnoty;
    }

    /**
     * @return the stlpce
     */
    public ArrayList<String> getStlpce() {
        return stlpce;
    }

    /**
     * @param stlpce the stlpce to set
     */
    public void setStlpce(ArrayList<String> stlpce) {
        this.stlpce = stlpce;
    }
    
    
}
