/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;


import java.awt.Frame;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author aaa
 */
public class vytvorenieTriedy 
{
    ResultSetMetaData met;
    
    
    public int insert (poziadavka poz)
    {
        ArrayList<tabulka_stlpce> vst = poz.getTabulky();
        String tabulka;
        ArrayList<String> stlpce;
        ArrayList<String> hodnoty;
        String upozornenie = "Atributy: ";
        
        
        int a=0;

        Class c;
        Object novy;
        int index = -1;
        int i=0;
        int warning = 0;
        int b=0;
        Field f;
        
        ArrayList<Object> objekty = new ArrayList<>();
        for (tabulka_stlpce v : vst)
        {
                upozornenie = "Atributy: ";
                tabulka = v.getTabulka();
                stlpce = v.getStlpce();
                hodnoty = v.getHodnoty();
               
                
                try
                {
                    c = Class.forName("tabulky."+tabulka);
                    novy = c.newInstance();
                }
                catch(Exception e)
                {
                    System.out.println("ZE NENAJDENE: tabulky."+tabulka);
                    System.out.println(e.toString());
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(new Frame(),"CHYBA: "+ e.toString()+"\n\nAk sa toto objavi, vsetko je v poriadku");
                    return 1;
                }
                
                b=0;
                for(String s : stlpce) 
                {
                       if((f = obsahuje(s, novy))==null)
                       {
                           warning++;
                           b++;
                           upozornenie+="\""+s+"\", ";
                           continue;
                       }
                       
                       System.out.println(stlpce.get(b));
                       if(priradHodnotuDoZakladnychTypov(novy,f, hodnoty.get(b))!=0)
                           return 3;
                       b++;
                       
                }        
                upozornenie+="neexistuju.\n Mozno nie je aktualny formular.";
                if(warning!=0)
                    JOptionPane.showMessageDialog(new Frame(),upozornenie);
                warning=0;
               
                for(int jjjj = 0;jjjj<poz.getJoin().size();jjjj++)
                {
                    
                    try
                    {
                        String pr = "from "+poz.getJoin().get(jjjj)+" t where t."+poz.getOn_1().get(jjjj)+" = '"+poz.getOn_2().get(jjjj)+"'";
                        ArrayList l = new ArrayList();
                        l.clear();
                        Session session = new Configuration().configure().buildSessionFactory().openSession();
                        session.beginTransaction();
                        Query query = session.createQuery(pr);
                        System.out.println(pr);
                        l = (ArrayList)query.list();
                        
                        if(l.isEmpty())
                        {
                            JOptionPane.showMessageDialog(new JFrame(), "Zly udaj zadavate do "+poz.getJoin().get(jjjj));
                            return -3;
                        }
                        
                        novy.getClass().getField(sprav_male(poz.getJoin().get(jjjj))).set(novy, l.get(0));
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                
                objekty.add(novy);
                i++;
                
        } 
        
//        for( Object ob : objekty)
//        {
//            index = -1;
//            String meno = ob.getClass().getName();
//            if((index = hlavne.mena_listov.indexOf(meno))==-1)
//                {
//                    hlavne.mena_listov.add(meno);
//                    hlavne.listy.add(new ArrayList<Object>());
//                    hlavne.listy.get(hlavne.listy.size()-1).add(ob);
//                    System.out.println("pridavam 1. raz "+meno);
//                }
//                else
//                {
//                    hlavne.listy.get(index).add(ob);
//                    System.out.println(meno+" tu uz je");
//                }
//        }
        for(Object ob: objekty)
            hlavne.hl.insert(ob);
        return 0;
    }
    
    
    public odpoved_selectu spracujRady2(ScrollableResults r, ArrayList<String> stl, odpoved_selectu os)
    {
        ArrayList<ArrayList<String>> zobrazi = new ArrayList<>();
        ArrayList<String> mena_stlpcov = new ArrayList<>();
        String [][] objekty3 = null;
        String [] objekty2 = null;
        int poc1=0;
        int poc2=0;
        int poc3=0;
        int i;
        int ggg=0;
        
        
        

            if(stl!=null)
            {
                System.out.println("pridavam si stlpce");
                mena_stlpcov.addAll(stl);
            }
            i=1;
            
            ggg = mena_stlpcov.size();
            objekty2 = new String[ggg];
            i=0;
            for(String s : mena_stlpcov)
            {
                System.out.println("stlpec: "+s);
                objekty2[i] = s;
                i++;
            }
            
            i=1;
            
            poc1=0;
            while(r.next())
            {
                //ScrollableResults je blbost, lebo nic nevie
                i=0;
                System.out.println(poc1+". cyklus");
                zobrazi.add(new ArrayList<String>());
                  for(i=0;i<ggg;i++)
                  {
                      System.out.println("Retazec: "+r.get(i));
                      try{
                        
                           zobrazi.get(poc1).add(0, r.get(i).toString());
                        }
                        catch(NullPointerException e)
                        {
                            zobrazi.get(poc1).add(0, null);
                            System.out.println(e.toString());
                        }
                  }
                  
                 poc1++;
            };
            if(!os.extra_stlpce.isEmpty())
            {
                os.extra_obsah = zobrazi;
            }
        //    if(!os.getNazvy_stlpcov().isEmpty())
          else
            {
                os.obsah = zobrazi;
            }
            return os;

    }
    
    
    
    
    public odpoved_selectu spracujRady(String s, ResultSet r, poziadavka poz)
    {
        Class c;
        Object novy;
        String meno;
        int kluc;
        int i=1;
        int index = -1;
        int pozicia = 0;
        int ggg=0;
        int ggg2=0;
        int gggindex = -1;
        int pocetRadov = 0;
        int pocet_stlpcov = 0;
        String pomocny = null;
        String vypise;
        odpoved_selectu odpoved = new odpoved_selectu();        
        ArrayList<Object> objekty = new ArrayList<>();
        ArrayList<String> mena_stlpcov = new ArrayList();
        boolean nahradit = false;
        odpoved.setZobrazit(poz.isZobrazit());
        
        
        
        
        try
                {
                    c = Class.forName(s);
                    novy = c.newInstance();
                }
                catch(Exception e)
                {
                    System.out.println(e.toString());
                    e.printStackTrace();
                    return null;
                }
     try
     {
         
         ResultSetMetaData meta = r.getMetaData();
        
        ggg=meta.getColumnCount()+1;
        for(i=1;i<ggg;i++)
            odpoved.getNazvy_stlpcov().add(meta.getColumnName(i));
        i=1;
        pocet_stlpcov = ggg -1;
        ggg=0;
      //  System.out.println("POCET STLPCOV: "+pocet_stlpcov);
        
        
         if(!r.isBeforeFirst())
         {
             //ak nic nenasiel
             return odpoved;
         }
                 
          r.first();
         
                 
       //     r.getString(1);
         do
        {
            odpoved.getObsah().add(new ArrayList<String>());
            ggg=ggg2=0;
            nahradit=false;
            pocetRadov++;
            try
                {
                    c = Class.forName(s);
                    novy = c.newInstance();
                }
                catch(Exception e)
                {
                    System.out.println(e.toString());
                    e.printStackTrace();
                    return null;
                }
            i=1;
            vypise = "";
           // System.out.println("=========");
            for(Field atribut : novy.getClass().getFields()) 
                {
                         try
                         {
                             priradHodnotuDoZakladnychTypov(novy, atribut,  r.getString(i));
                             vypise+=atribut.getName().toString()+" ("+atribut.getType().toString()+") :"+atribut.get(novy).toString()+" -- "+r.getString(i)+"\n";                     
                             odpoved.getObsah().get(pocetRadov - 1).add(r.getString(i));
                         }
                         catch(NullPointerException e)
                         {
                             odpoved.getObsah().get(pocetRadov - 1).add("");
                         }
                         catch(SQLException e)
                         {
                             e.printStackTrace();
                             return null;
                         }
                     
                     
                     if((i==1) && (poz.isUlozit()))
                    {
                        try
                        {
                            index = hlavne.mena_listov.indexOf(s);
                            if(index == -1)
                                System.out.println("INDEX TU NEMOZE BYT -1");
                           
                            ggg=0;      //Ak nacitavam z databazy nejaky objekt, tak tu verziu, ktoru mam teraz v pamati vymazem
                                for(Object neviem : hlavne.listy.get(index))
                                {
                                    
                                    if(neviem.getClass().getFields()[0].getInt(neviem) == Integer.parseInt(r.getString(i)))
                                    {
                                      //  System.out.println("Ideme nahradzat "+atribut.getInt(novy)+" = "+Integer.parseInt(r.getString(i)));
                                        nahradit=true;
                                        ggg = hlavne.listy.get(index).indexOf(neviem);
                                        hlavne.listy.get(index).remove(ggg);
                                        gggindex = index;
                                        index = -1;
                                        ggg2 = ggg;
                                        break;
                                    }
                                    ggg++;
                                }
                                if(!nahradit)
                                {
                                    hlavne.listy.get(index).add(novy);
                                }
                        }
                        catch(Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                     
                     i++;
                     pozicia=i;
                }
            
                if(poz.getGroup_by()!=null)
                    odpoved.getObsah().get(pocetRadov - 1).add(r.getString(pozicia));
          //  System.out.println(vypise);
            if(nahradit)
            {
                //System.out.println("gggindex: "+gggindex+" ggg2: "+ggg2);
                hlavne.listy.get(gggindex).add(ggg2, novy);
                nahradit=false;
                ggg=0;
                ggg2=0;
                gggindex = -1;
            }
           objekty.add(novy);  
        }while(r.next());
       // System.out.println("POCET RIADKOV: "+pocetRadov);
     }
     catch(Exception e)
     {
         e.printStackTrace();
     }
     for(Object n : objekty)
     {
            index= -1;
            meno = n.getClass().getName().toString();
            if((index = hlavne.mena_listov.indexOf(meno))==-1)
                {
                    hlavne.mena_listov.add(meno);
                    hlavne.listy.add(new ArrayList<Object>());
                    hlavne.listy.get(hlavne.listy.size()-1).add(n);
                }
                else
                {
                        hlavne.listy.get(index).add(n);
                }
     }
        odpoved.setObjekty(objekty);
        if(!objekty.isEmpty())
            odpoved.setPrazdne(false);
        odpoved.setStlpce(mena_stlpcov);
        return odpoved;
    }
    
    
    public int delete(poziadavka poz)
    {
        String tabulka = poz.getTabulky().get(0).getTabulka();
        int pk = poz.getPk();
        Object o = null;
        int pozicia = -1;
        int zaznam = -1;
        int pocet;
        int i=0;
        if(pk<=0)
        {
            JOptionPane.showMessageDialog(new Frame(),"Zaznam so zadanym Primary key-om ("+pk+") neexistuje");
        }
//        for(i=0;i<hlavne.POCET_KRABICIEK;i++)
//        {
//            if(hlavne.mena_listov.get(i).equals("tabulky."+tabulka))
//            {
//                break;
//            }
//        }
//        if(i==hlavne.POCET_KRABICIEK)
//        {
//            System.out.println("CHYBA: tato tabulka by tu ani nemala byt: "+tabulka);
//            return -1;
//        }
//        pozicia=i;
//        pocet = hlavne.listy.get(pozicia).size();
//        for(i=0;i<pocet;i++)
//        {
//            o = hlavne.listy.get(pozicia).get(i);
//            try
//            {
//                if(o.getClass().getFields()[0].getInt(o) == pk)
//                {
//                    zaznam=i;
//                    break;
//                }
//            }
//            catch(Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//        if(i==pocet)
//        {
//            JOptionPane.showMessageDialog(new Frame(),"Zaznam so zadanym Primary key-om ("+pk+") neexistuje");
//            return -2;
//        }
//        if(o==null)
//        {
//            System.out.println("CHYBA: Neexistuje tento objekt");
//            return -3;
//        }
//        hlavne.listy.get(pozicia).remove(zaznam);
        hlavne.hl.delete(o, poz);
        
        
        return 0;
    }
    
    public int update(poziadavka poz)
    {
        String tabulka = poz.getTabulky().get(0).getTabulka();
        int pk = poz.getPk();
        Object o = null;
        
        hlavne.hl.update(poz);
//        if(delete(poz)<0)
//        {
//            JOptionPane.showMessageDialog(new Frame(),"Update sa nepodaril, zlyhal v casti delete");
//            return 1;
//        }
//        if(insert(poz)!=0)
//        {
//            JOptionPane.showMessageDialog(new Frame(),"Update sa nepodaril, zlyhal v casti insert");
//            return 2;
//        }
        return 0;
    }
    
    
    public ArrayList<Object> select(poziadavka poz)
    {
        String tabulka;
        odpoved_selectu vysledok;
        int l = -1;
        Class c;
        vysledok = hlavne.hl.select2(poz);
        System.out.println("idem zobrazovat");
        zobrazit(vysledok);
        
        if(vysledok==null)
            return null;
        return vysledok.getObjekty();
    }
    
    
    
    public Field obsahuje( String s, Object ob)
    {
        for(Field atribut : ob.getClass().getFields()) 
            {
                 if(atribut.getName().equals(s))
                     return atribut;
            }
        return null;
    }
    
    public String zistiTyp(Field fl)
    {
	return fl.getType().toString();       
    }
    
    
    public int priradHodnotuDoZakladnychTypov(Object o, Field atribut, String hodnota)
    {
        
        
        
        if(hodnota == null)
            return 0;
        String typ = zistiTyp(atribut);
        
        if (typ.equals("class java.sql.Time") || typ.equals("class java.sql.Date"))
        {
            
            //System.out.println("ZISTENY TYP: "+typ);
        }
       
        
        try
        {
            System.out.println("typ: "+typ+" .. hodnota: "+hodnota+" sk. typ: "+hodnota.getClass().getName());
            Object ob = konvertuj(typ, hodnota);
            atribut.set(o, ob);
        }
        catch(NumberFormatException e)
        {
            JOptionPane.showMessageDialog(new Frame(),"CHYBA: "+ e.toString()+"\n\nZadavate zly format vstupu pri \""+hodnota+"\"\nMa tam byt typ "+typ);
            return 1;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return 2;
        }
        return 0;
  
    }
    
  public static Object konvertuj( String typ, String hodnota ) throws NumberFormatException 
  {
             
        DateFormat df;
        if(typ.equals("class java.lang.String"))
            return hodnota;
            if(typ.equals("class java.lang.Integer"))
                return Integer.valueOf(hodnota);
            if(typ.equals("class java.lang.Double"))
                return Double.valueOf(hodnota);
            if(typ.equals("class java.lang.Long"))
                return Long.valueOf(hodnota);
             if(typ.equals("int"))
                return Integer.parseInt( hodnota );
            if(typ.equals("long"))
                return Long.parseLong( hodnota );
            if(typ.equals("double"))
                return Double.parseDouble( hodnota );
            if(typ.equals("class java.sql.Date"))
            {
                /*
                try
                {
                    return  new SimpleDateFormat("YYYY-MM-DD").parse(hodnota);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
                * */
                java.sql.Date datum = java.sql.Date.valueOf(hodnota);
                return datum;
            }
            if(typ.equals("class java.sql.Time"))
            {
                java.sql.Time cas = java.sql.Time.valueOf(hodnota);
                return cas;
            }
        
        return hodnota;
    }

    /**
     * @return the pokracovat
     */
     public void zobrazit(odpoved_selectu os)
     {
         String [][] objekty3 = null;
        String [] objekty2 = null;
        ArrayList<Object> potom = new ArrayList<>();
        int i=0;
        int ggg=0;
        int poc1=0;
        int poc2=0;
        int poc3=0;

      if(os==null)
      {
          System.out.println("Nenasiel ziadne objekty");
          return;
      }
        System.out.println("zacinam zobrazovat");
        ggg= os.getNazvy_stlpcov().size();
        poc1=os.getObsah().size();
        objekty3 = new String[poc1][ggg + os.extra_stlpce.size()]; 
          for(ArrayList<String> a : os.getObsah())
          {
              poc3=0;
              System.out.println("s=================");
              for(String s : a)
              {
                  System.out.println(s);
                  objekty3[poc2][poc3] = s;
                  poc3++;
              }
              poc2++;
          }
        if(poc2==0)
        {
            System.out.println("ide ziskavat objekty");
            poc1 = os.getObjekty().size();
            objekty3 = new String[poc1][ggg + os.extra_stlpce.size()]; 
            for(Object a : os.getObjekty())
            {
                potom.clear();
                poc3=0;
              System.out.println("u=================");
                for(Field f : a.getClass().getFields())
                {
                        if(os.getNazvy_stlpcov().size()<=poc3)
                        {
                           
                            break;
                        }
                    //   System.out.println("f.getname(): "+f.getName()+" .. stlpec: "+os.getNazvy_stlpcov().get(poc3)+" .. poc3: "+poc3);

                        if(f.getName().equals(os.getNazvy_stlpcov().get(poc3)))
                        {
                            try
                            {
                                System.out.println("TYP TRIEDY: "+f.get(a).getClass().getName());
                                System.out.println(f.get(a).toString());
                                objekty3[poc2][poc3] = f.get(a).toString();
                                poc3++;
                            }
                            catch(NullPointerException e)
                            {
                                poc3++;
         //                       System.out.println(e.toString());
                            }
                            catch(Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            try
                            {
                //                System.out.println("TYP TRIEDY 2: "+f.get(a).getClass().getName());
                            
                                if(!f.get(a).getClass().getName().equals("org.hibernate.collection.PersistentSet"))
                                {
                                    potom.add(f.get(a));
                                }
                            }
                            catch(NullPointerException e)
                            {
                                System.out.println(e.toString());
                            }
                            catch(Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                    
                for(Object p : potom)
                {
               //     System.out.println("--- p: "+p.getClass().getName());
                    for(Field f : p.getClass().getFields())
                    {
                        
                         if(os.getNazvy_stlpcov().size()<=poc3)
                        {
                           
                            break;
                        }
      //                  System.out.println("f.getname(): "+f.getName()+" .. stlpec: "+os.getNazvy_stlpcov().get(poc3)+" .. poc3: "+poc3);

                        if(f.getName().equals(os.getNazvy_stlpcov().get(poc3)))
                                {
                                    try
                                    {
                              //          System.out.println("MENO: "+f.getName()+" .. TYP TRIEDY: "+f.get(p).getClass().getName());
                                        //System.out.println(f.get(a).toString());
                                        objekty3[poc2][poc3] = f.get(p).toString();
                                        poc3++;
                                    }
                                    catch(NullPointerException e)
                                    {
                                        poc3++;
        //                                System.out.println(e.toString());
                                    }
                                    catch(Exception e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                    }
                }
                
                
                poc2++;
            }
        }
        if(!os.extra_obsah.isEmpty())
                {
                    int poc4=0;
                    try
                    {
                        for(String s : os.extra_obsah.get(poc2))
                        {
                            System.out.println("EXTRA OBSAH: "+s+" .. poc2: "+poc2);
                            objekty3[poc2][poc3] = s;
                            poc4++;
                        }
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
        i=0;
        objekty2 = new String[ggg + os.extra_stlpce.size()];
         for(String s : os.getNazvy_stlpcov())
            {
                objekty2[i] = s;
                i++;
            }
         for(String s : os.extra_stlpce)
         {
             objekty2[i] = s;
             i++;
         }
         
         hlavne.gt.set(objekty3, objekty2);
         

         
         
     }
     
     public String konvertuj_na_string(String s)
     {
         return s;
     }
     public String konvertuj_na_string(int i)
     {
         return Integer.toString(i);
     }
     public String konvertuj_na_string(Double d)
     {
         return Double.toString(d);
     }
     public String konvertuj_na_string(boolean b)
     {
         return Boolean.toString(b);
     }
     
  public String sprav_male(String s)
    {
        char c = s.charAt(0);
        if(c>='A' && c<='Z')
        {
            c = (char)(c + ('a' - 'A'));
        }
        s = s.substring(1);
        s = c+s;
        return s;
    }
     
     
}
