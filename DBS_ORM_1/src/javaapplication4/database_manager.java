/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

// import com.mysql.jdbc.Connection;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;



/**
 *
 * @author aaa
 */
public class database_manager {

    /**
     * @param args the command line arguments
     */
    
    Statement dotaz;
    Statement citanie = null;
    Statement mazanie = null;
    Statement aktualizacia = null;
    PreparedStatement ukladanie = null;
    ResultSet vysledok;
    String url;
    String password;
    String username;
    String prikaz = "";
    Connection spojenie = null;
    
    
    public int ukonci()
    {
        
        if (spojenie != null) try { spojenie.close();} catch (SQLException e) {e.printStackTrace();return 1;}
        return 0;
    }
    
    
    public int insert(Object ob) {
        
        try {
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        session.beginTransaction();
        session.save(ob);
        session.flush();
        session.refresh(ob);
        session.getTransaction().commit();
        session.close();
        System.out.println(ob.getClass().getName());
    } catch (HibernateException he) {
        he.printStackTrace();
    }
        System.out.println("Closing the connection.");
        //ukonci();
        return 0;
    }
    
    public ResultSet inicializuj()
    {     
      pripravSpojenie();
      try
      {
          vysledok = citanie.executeQuery("show tables");
          while(vysledok.next())
            {
                hlavne.listy.add(new ArrayList<>());
                hlavne.mena_listov.add("tabulky."+vysledok.getString(1));
                hlavne.POCET_KRABICIEK++;
            }
          
      }
      catch(SQLException e)
      {
          System.out.println(e.toString());
        //  e.printStackTrace();
          ukonci();
      }
      
         
      
          for(int i=0;i<hlavne.POCET_KRABICIEK;i++)
          {
              
              try
              {
                  String vyber = hlavne.mena_listov.get(i).substring(hlavne.mena_listov.get(i).lastIndexOf('.')+1);
                    vysledok = citanie.executeQuery("SELECT * FROM "+vyber);
                    
                        hlavne.vt.spracujRady(hlavne.mena_listov.get(i), vysledok, new poziadavka(true, false));
                    
              }
              catch(SQLException e)
                {
                    System.out.println(e.toString());
                    continue;
                }
              
          }
      
      
        ukonci();
        return vysledok;
    }
    
    public odpoved_selectu select(poziadavka poz)
    {
       String tabulka = null;
       ArrayList<tabulka_stlpce> vst = poz.getTabulky();
       String pripravenyPrikaz;
       String op = poz.getOp();
       odpoved_selectu odpoved = new odpoved_selectu();
       ResultSet vysledok2 = null;
        ArrayList<String> stlpce;
        ArrayList<String> hodnoty;
        ArrayList<Object> objekty = new ArrayList<>();
        int poc1=0;
        int special = 0;
        int odkial_vymazat = 0;
        
        pripravSpojenie();
        
            
            prikaz="";
            
            if((pripravenyPrikaz = poz.getPripravenyPrikaz())!=null)
            {
                prikaz=pripravenyPrikaz;
                char c;
                int poc2=0;
                int poc3=0;
                try
                {
                    try
                    {
                        while(true)     //Na vyskocenie z tohoto cyklu zneuzivam vynimku
                        {
                                if((c=prikaz.charAt(poc2))=='?')
                               {
                                   special = 0;
                                   poc3=poc2;
                                   poc2++;
                                   c = prikaz.charAt(poc2);
                                   while((c!=' ') && (c!=';') && (c!='\''))
                                   {
                                       
                                       special= special * 10 + c - '0';
                                       poc2++;
                                       c = prikaz.charAt(poc2);
                                       
                                   }
                                   //System.out.println("Special: "+special);
                                   prikaz = prikaz.substring(0,poc3) + poz.getDoplnkove().get(special - 1) + prikaz.substring(poc2);
                                   prikaz+=" ";
                               }
                               if(c==';')
                                   break;
                              poc2++;
                        }
                        prikaz+=";";
                    }
                
                    catch(Exception e)
                    {
                        //e.printStackTrace();
                        System.out.println(e.toString());
                    }
                    System.out.println("PRIKAZ: "+prikaz);
                    vysledok = citanie.executeQuery(prikaz);
             //       hlavne.vt.spracujRady2(vysledok, poz);
                    ukonci();
                    return null;
                    
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    ukonci();
                    return null;
                }
                
            }
            else
            {
                    tabulka_stlpce v = vst.get(0);
                    tabulka = v.getTabulka();
                    stlpce = v.getStlpce();
                    hodnoty = v.getHodnoty();

                    prikaz+=" FROM "+tabulka+" t1 ";
  
                    
 
                    
                    
                        if(!hodnoty.isEmpty())
                            prikaz+=" WHERE ";

                        for (String stl : stlpce)
                        {
                           if((hodnoty.get(poc1)!=null))
                           {
                               prikaz+=" t1."+stl+"= :par ";
                        //       prikaz+=" t1."+stl+"='"+hodnoty.get(poc1)+"' "+op+" ";
                           }
                           poc1++;
                        }

//                        if(poz.isCela_tabulka())
//                            {
//                                prikaz+=" 1=1 OR 1=1;;";
//                            }
                       

                        //prikaz = prikaz.trim();
                        if(prikaz.lastIndexOf("AND")>prikaz.lastIndexOf("OR"))
                        {
                            odkial_vymazat = prikaz.lastIndexOf("AND");
                        }
                        else
                        {
                            odkial_vymazat = prikaz.lastIndexOf("OR");
                        }
                        
                        if(v.je_nejaka_hodnota())
                            prikaz = prikaz.substring(0, odkial_vymazat);
                        //prikaz = prikaz.trim();
                        else
                        {
                            prikaz = prikaz.substring(0, prikaz.length() - 6);
                        }
                        

                    if(poz.getGroup_by()!=null)
                    {
                        if(!poz.isCela_tabulka() && hodnoty.isEmpty())
                            prikaz = prikaz.substring(0, prikaz.length()-2);
                        prikaz+=" GROUP BY "+poz.getGroup_by();
                    }

                    prikaz+=";";
            }
            
            System.out.println("PRIKAZ PRE SELECT: "+prikaz);
            

            try
            {
              vysledok = citanie.executeQuery(prikaz);
              vysledok2 = vysledok;
              poc1=0;
             
                    
                 //   System.out.println("1 VRATIL: "+odpoved.getObjekty().size());

            }
             catch(SQLException e)
              {
                  System.out.println(e.toString());
                //  e.printStackTrace();
                  ukonci();
              }
        poc1++;
        
        if(poz.getJoin().size()==0)
            odpoved = hlavne.vt.spracujRady("tabulky."+tabulka, vysledok, poz); 
        else
        {
   //         hlavne.vt.spracujRady2(vysledok, poz);
            ukonci();
            return null;
        }
        ukonci();
        odpoved.setZobrazit(poz.isZobrazit());
        odpoved.setZ_jednej_tabulky(poz.isZ_jednej_tabulky());
        return odpoved;
    }
    
    
    
    public odpoved_selectu select2(poziadavka poz)
    {
        String tabulka = null;
        String tabulka_male = null;
       ArrayList<tabulka_stlpce> vst = poz.getTabulky();
       String pripravenyPrikaz;
       String op = poz.getOp();
       odpoved_selectu odpoved = new odpoved_selectu();
       ResultSet vysledok2 = null;
        ArrayList<String> stlpce;
        ArrayList<String> hodnoty;
        ArrayList<Object> objekty = new ArrayList<>();
        int poc1=0;
        int poc2=0;
        int special = 0;
        int odkial_vymazat = 0;
        String pr2;
        
        
        
        prikaz = "";
        
        
//        if((pripravenyPrikaz = poz.getPripravenyPrikaz())!=null)
//        {
//            
//        }
                
        if((pripravenyPrikaz = poz.getPripravenyPrikaz())!=null)
        
        {
                prikaz=pripravenyPrikaz;
                char c;
                int poc22=0;
                int poc3=0;
                try
                {
                    try
                    {
                        while(true)     //Na vyskocenie z tohoto cyklu zneuzivam vynimku
                        {
                                if((c=prikaz.charAt(poc2))=='?')
                               {
                                   special = 0;
                                   poc3=poc2;
                                   poc2++;
                                   c = prikaz.charAt(poc2);
                                   while((c!=' ') && (c!=';') && (c!='\''))
                                   {
                                       
                                       special= special * 10 + c - '0';
                                       poc2++;
                                       c = prikaz.charAt(poc2);
                                       
                                   }
                                   //System.out.println("Special: "+special);
                                   prikaz = prikaz.substring(0,poc3) + poz.getDoplnkove().get(special - 1) + prikaz.substring(poc2);
                                   prikaz+=" ";
                               }
                               if(c==';')
                                   break;
                              poc2++;
                        }
                    //    prikaz+=";";
                        prikaz = prikaz.substring(0, prikaz.lastIndexOf(";"));
                    }
                
                    catch(Exception e)
                    {
                        //e.printStackTrace();
                        System.out.println(e.toString());
                    }
                    System.out.println("PRIKAZ: "+prikaz);
        
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    
                }
                
                
                Session session = new Configuration().configure().buildSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery(prikaz);
        
        ArrayList<Object> objekt = new ArrayList<>();
        
        
        
        
        
        ScrollableResults scr = query.scroll();
        ArrayList<String> akozeStlpce = new ArrayList<>();
        akozeStlpce.add("Vychodzie letisko");
        akozeStlpce.add("cislo linky");
        akozeStlpce.add("cielove letisko");
        //odpoved.extra_stlpce.addAll(akozeStlpce);
        odpoved.nazvy_stlpcov.addAll(akozeStlpce);
       odpoved = hlavne.vt.spracujRady2(scr, odpoved.getNazvy_stlpcov(), odpoved);      //DORIESIT
        //odpoved.setObjekty((ArrayList)query.list());
   //     odpoved.setObjekty(objekty);
        
        return odpoved;
            }
        
        
        
        
        
        tabulka_stlpce v = vst.get(0);
        tabulka = v.getTabulka();
        tabulka_male = tabulka.substring(1);
        tabulka_male = tabulka.charAt(0)+tabulka_male;
        stlpce = v.getStlpce();
        hodnoty = v.getHodnoty();
        
        odpoved.getNazvy_stlpcov().addAll(stlpce);
        
        prikaz+=" FROM "+tabulka+" v ";
        
        for(int j=0; j<poz.getJoin().size();j++)
        {
            //from Lietadlo l, TypLietadla tl where l.typLietadla = tl AND tl.typL = '737'
          //  prikaz = prikaz +" JOIN "+poz.getJoin().get(j)+" ON "+poz.getOn_1().get(j)+"="+poz.getOn_2().get(j)+" ";
            prikaz+=" join fetch v."+sprav_male(poz.getJoin().get(j))+" t"+j+" ";
        }
        
        if(!hodnoty.isEmpty())
               prikaz+=" WHERE ";
        for(int j=0; j<poz.getJoin().size();j++)
        {
            prikaz+=" v."+sprav_male(poz.getJoin().get(j))+" = t"+j+" AND ";
            prikaz+=" t"+j+"."+poz.getOn_1().get(j)+" = '"+poz.getOn_2().get(j)+"' AND ";
           // System.out.println("MENO: "+Class.forName(poz.getJoin().get(j)).getFields());
            try
            {
                for(Field fl : Class.forName("tabulky."+poz.getJoin().get(j)).getFields())
                    odpoved.getNazvy_stlpcov().add(fl.getName());
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        
        //prikaz = "select "+prikaz;
        
        
        
        
        try
        {
       //     prikaz=prikaz.substring(0, prikaz.lastIndexOf("AND"));
        }
        catch(StringIndexOutOfBoundsException e)
        {
            
        }
        poc1=0;
        poc2=0;
        prikaz+=" (";
        for (String stl : stlpce)
        {
       //     System.out.println(prikaz);
           if((hodnoty.get(poc1)!=null))
           {
               prikaz+=" v."+stl+" = '"+hodnoty.get(poc1)+"' "+op+" ";
               poc2++;
           }
           poc1++;
        }
        
        if(prikaz.lastIndexOf("AND")>prikaz.lastIndexOf("OR"))
        {
            odkial_vymazat = prikaz.lastIndexOf("AND");
        }
        else
        {
            odkial_vymazat = prikaz.lastIndexOf("OR");
        }

        if(odkial_vymazat != -1)
            prikaz = prikaz.substring(0, odkial_vymazat);
        //prikaz = prikaz.trim();
        else
        {
            prikaz = prikaz.substring(0, prikaz.lastIndexOf("("));
            if(poz.getJoin().isEmpty())
                prikaz = prikaz.substring(0, prikaz.lastIndexOf("WHERE"));
        }
        if(poc2!=0)
            prikaz+=") "; 
        
         if(poz.getGroup_by()!=null)
                    {
                        pr2 = "";
                        for(String st : odpoved.getNazvy_stlpcov())
                        {
                            pr2 = st+", "+pr2; 
                        }
                        pr2=pr2.substring(0, pr2.length()-2);
                        pr2+=", COUNT(*) ";
                        prikaz = "select "+pr2+" "+prikaz;
                        
                        if(!poz.isCela_tabulka() && hodnoty.isEmpty())
                            prikaz = prikaz.substring(0, prikaz.length()-2);
                        prikaz+=" GROUP BY "+poz.getGroup_by();
                        
                        //pr2 = new String(prikaz);
                //        pr2 = "select COUNT("+poz.getGroup_by()+") "+pr2;
                        //pr2 = "select COUNT(*) "+pr2;
                        
                //        prikaz="select *, COUNT("+poz.getGroup_by()+") as pocet "+prikaz;
                //       odpoved.getNazvy_stlpcov().add("pocet");
                        Session session2 = new Configuration().configure().buildSessionFactory().openSession();
                        session2.beginTransaction();
                        System.out.println("2. select: "+pr2);
                        Query query2 = session2.createQuery(prikaz);
                        odpoved_selectu odp = new odpoved_selectu();
                        
                        ScrollableResults scr = query2.scroll();
                        System.out.println("COUNT("+poz.getGroup_by()+") ");
                 //       odpoved.nazvy_stlpcov.add(0, "COUNT("+poz.getGroup_by()+") ");
                        odpoved.nazvy_stlpcov.add(0, "Pocet");
                        //odpoved.extra_stlpce.add("COUNT("+poz.getGroup_by()+") ");
                        odpoved = hlavne.vt.spracujRady2(scr, odpoved.getNazvy_stlpcov(), odpoved);
                     //   return null;
                        
                    }
         
        System.out.println("HQL prikaz: "+prikaz);
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery(prikaz);
        
        
        
        
        
        objekty = (ArrayList)query.list();
        ScrollableResults sr = query.scroll();
        
//        if(poz.getGroup_by()!=null)
//            hlavne.vt.spracujRady2(sr, odpoved.getNazvy_stlpcov());
        
        //odpoved.setObjekty((ArrayList)query.list());
        odpoved.setObjekty(objekty);
        odpoved.setZobrazit(poz.isZobrazit());
//        System.out.println("Vypisem polia tohoto sprosteho listu: "+objekty.get(0).getClass().getName());
//        for(Field hovno : odpoved.getObjekty().get(0).getClass().getFields())
//        {
//            try
//            {
//                System.out.println(hovno.getName() +" ... "+hovno.get(odpoved.getObjekty().get(0)));
//            }
//            catch(Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
        return odpoved;
    }
    
    public String sprav_male(String s)
    {
        char c = s.charAt(0);
        if(c>='A' && c<='Z')
        {
            c = (char)(c + ('a' - 'A'));
        }
        s = s.substring(1);
        s = c+s;
        return s;
    }
    
    
    
    
    public int delete(Object o, poziadavka poz)
    {
        String tabulka = poz.getTabulky().get(0).getTabulka();
        int pk;
        String prikaz = "";
        String pkS;
        tabulka_stlpce ts = poz.getTabulky().get(0);
//        pripravSpojenie();
//        try
//        {
//            pk = o.getClass().getFields()[0].getInt(o);
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//            return -4;
//        }
//        pkS = Integer.toString(pk);
//        try
//        {
//            prikaz = "DELETE FROM "+tabulka+" WHERE "+o.getClass().getFields()[0].getName()+" = "+pkS+";";
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//            return -5;
//        }
//        
//        System.out.println("VYKONA SA TAKYTO PRIKAZ DELETE: "+prikaz);
//        try
//        {
//            mazanie=spojenie.prepareStatement(prikaz);
//            mazanie.executeUpdate(prikaz);
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//            return -6;
//        }
//        
        String primary_name;
        String primary_value;
        Integer primary_key;
        String s1 = ts.getStlpce().get(0);
            if(s1.substring(0, 2).equals("id") && s1.substring(s1.length()-2).equals("Pk"))
            {
                primary_name = s1;
                primary_value = ts.getHodnoty().get(0);
               try
               {
                   primary_key = Integer.decode(primary_value);
               }
               catch(NumberFormatException e)
               {
                   e.printStackTrace();
                   return 1;
               }
                
               // continue;
            }
            else
            {
                 JOptionPane.showMessageDialog(new JFrame(), "Vsetko je OK");
                 return 1;
            }
            
            Session session = new Configuration().configure().buildSessionFactory().openSession();
            Transaction tr = session.beginTransaction();
            //String dt = poz.getTabulky().get(0).getTabulka().substring(poz.getTabulky().get(0).getTabulka().lastIndexOf("."));
            String hql = "delete from "+poz.getTabulky().get(0).getTabulka()+" l where l."+s1+" = '"+primary_value+"'";
            System.out.println("HQL: "+hql);
            Query query = session.createQuery(hql);
            query.executeUpdate();
            tr.commit();
            
            //Object mazany = new Object();
//            try
//            {
//         //       mazany = session.load(Class.forName("tabulky."+poz.getTabulky().get(0).getTabulka()), primary_key);
//            }
//           catch(Exception e)
//           {
//               e.printStackTrace();
//               return 2;
//           }
//       //     session.delete(mazany);
            
        return 0;
    }
    
    
    public int update(poziadavka poz)
    {
        String tabulka = poz.getTabulky().get(0).getTabulka();
        Object aktualizovany = null;
        String prikaz = "";
        int i=0;
        int b=0;
        String primary_name = "";
        String primary_value = "";
        Integer primary_key;
        tabulka_stlpce ts = poz.getTabulky().get(0);
        
 //       pripravSpojenie();
        
 //       prikaz = "UPDATE "+tabulka+" SET ";
       String s1 = ts.getStlpce().get(0);
            if(s1.substring(0, 2).equals("id") && s1.substring(s1.length()-2).equals("Pk"))
            {
                primary_name = s1;
                primary_value = ts.getHodnoty().get(0);
                i++;
               // continue;
            }
            else
            {
                 JOptionPane.showMessageDialog(new JFrame(), "Vsetko je OK");
                 return 1;
            }
//            if(ts.getHodnoty().get(i)!=null)
//            {
//                if(b!=0)
//                    prikaz+=", ";
//                prikaz = prikaz + s1 + " = '"+ts.getHodnoty().get(i)+"' ";
//                b++;
//            }
//            i++;
        
        try
        {
            primary_key = Integer.decode(primary_value);
        }
        catch(NumberFormatException e)
        {
            JOptionPane.showMessageDialog(new JFrame(), "Zly vstup na primary key");
            return 1;
        }
        
        Session session = new Configuration().configure().buildSessionFactory().openSession();
        
        try
        {
            aktualizovany = session.get(Class.forName("tabulky."+poz.getTabulky().get(0).getTabulka()), primary_key);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        i=0;
        for(String s2 : ts.getStlpce())
        {
            if(ts.getHodnoty().get(i)!=null)
                {
                    try
                    {
                        if(aktualizovany==null)
                        {
                            System.out.println("AKTUALIZOVANY = NULL");
                            
                        }
                       // aktualizovany.getClass().getField(sprav_male(poz.getTabulky().get(0).getTabulka())).set(aktualizovany, konvertuj(aktualizovany.getClass().getField(sprav_male(poz.getTabulky().get(0).getTabulka())).getType().toString(),ts.getHodnoty().get(i)));
                          aktualizovany.getClass().getField(s2).set(aktualizovany, konvertuj(aktualizovany.getClass().getField(s2).getType().toString(),ts.getHodnoty().get(i)));
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            i++;
        }
        session.beginTransaction();
        session.save(aktualizovany);
        session.flush();
        session.refresh(aktualizovany);
        session.getTransaction().commit();
        session.close();
        
        
//        prikaz+="WHERE "+primary_name+"= '"+primary_value+"';";
//        System.out.println("UPDATE: "+prikaz);
//        try
//        {
//            aktualizacia = spojenie.prepareStatement(prikaz);
//            aktualizacia.executeUpdate(prikaz);
//        }
//        catch(Exception e)
//        {
//            e.printStackTrace();
//            ukonci();
//            return -6;
//        }
//        ukonci();
        return 0;
    }
    
    
    
    public int pripravSpojenie()
    {
        ukladanie = null;
        prikaz = "";
        vysledok = null;
        citanie = null;
        mazanie = null;
        aktualizacia = null;
       try 
            {
                
                Class.forName("com.mysql.jdbc.Driver");
                
            }
       catch (ClassNotFoundException e) 
            {
                throw new RuntimeException("Cannot find the driver in the classpath!", e);
            }
       
        url = "jdbc:mysql://localhost:3306/"+hlavne.meno_databazy;
        username = hlavne.meno;
        password = hlavne.heslo;
        spojenie = null;
        try {
           
            spojenie = DriverManager.getConnection(url, username, password);
           
            citanie = spojenie.createStatement();
            System.out.println("Uspesne prihlasenie!");
        } catch (SQLException e) {
            System.out.println("Neuspesne prihlasenie, pravdepodobne zle meno, alebo heslo");
            System.out.println(e.toString());
            if(spojenie == null)
            {
                return 3;
            }
            if (spojenie != null) try { spojenie.close(); return 1;} catch (SQLException ignore) {return 2;}
            System.exit(1);
        }
        return 0;
    }
    
    public static Object konvertuj( String typ, String hodnota ) throws NumberFormatException 
  {
             
        DateFormat df;
        if(typ.equals("class java.lang.String"))
            return hodnota;
            if(typ.equals("class java.lang.Integer"))
                return Integer.valueOf(hodnota);
            if(typ.equals("class java.lang.Double"))
                return Double.valueOf(hodnota);
            if(typ.equals("class java.lang.Long"))
                return Long.valueOf(hodnota);
             if(typ.equals("int"))
                return Integer.parseInt( hodnota );
            if(typ.equals("long"))
                return Long.parseLong( hodnota );
            if(typ.equals("double"))
                return Double.parseDouble( hodnota );
            if(typ.equals("class java.sql.Date"))
            {
                /*
                try
                {
                    return  new SimpleDateFormat("YYYY-MM-DD").parse(hodnota);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
                * */
                java.sql.Date datum = java.sql.Date.valueOf(hodnota);
                return datum;
            }
            if(typ.equals("class java.sql.Time"))
            {
                java.sql.Time cas = java.sql.Time.valueOf(hodnota);
                return cas;
            }
        
        return hodnota;
    }
    
}
