/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

import java.util.ArrayList;

/**
 *
 * @author aaa
 */
public class poziadavka {
    
    public String typ_poziadavky;
    public String op;
    public boolean ulozit;
    public String pripravenyPrikaz;
    public ArrayList<tabulka_stlpce> tabulky = new ArrayList<>();
    public ArrayList<String> doplnkove;
   public boolean zobrazit;
   public boolean z_jednej_tabulky;
   public int pk;
   public String group_by;
   public boolean cela_tabulka;
   public ArrayList<String> join = new ArrayList<>();
   public ArrayList<String> on_1 = new ArrayList<>();
   public ArrayList<String> on_2 = new ArrayList<>();
   public ArrayList<Object> join2; 
    
    public poziadavka()
    {
        this.vynuluj();
        doplnkove = new ArrayList<>();
    }
    public poziadavka(boolean a, boolean b)
    {
        this.vynuluj();
        this.setUlozit(a);
        this.setZobrazit(b);
        doplnkove = new ArrayList<>();
    }
    
    public void vynuluj()
    {
        getJoin().clear();
        getOn_1().clear();
        getOn_2().clear();
        setTyp_poziadavky(op = pripravenyPrikaz = null);
        setUlozit(false);
        getTabulky().clear();
        setZobrazit(true);
        setZ_jednej_tabulky(true);
        setPk(-1);
        setGroup_by(null);
        setCela_tabulka(false);
        setDoplnkove(null);
        setDoplnkove(new ArrayList<String>());
    }
    
    
    
    
    
    /**
     * @return the typ_poziadavky
     */
    public String getTyp_poziadavky() {
        return typ_poziadavky;
    }

    /**
     * @param typ_poziadavky the typ_poziadavky to set
     */
    public void setTyp_poziadavky(String typ_poziadavky) {
        this.typ_poziadavky = typ_poziadavky;
    }

    /**
     * @return the op
     */
    public String getOp() {
        return op;
    }

    /**
     * @param op the op to set
     */
    public void setOp(String op) {
        this.op = op;
    }

    /**
     * @return the ulozit
     */
    public boolean isUlozit() {
        return ulozit;
    }

    /**
     * @param ulozit the ulozit to set
     */
    public void setUlozit(boolean ulozit) {
        this.ulozit = ulozit;
    }

   
    /**
     * @return the pripravenyPrikaz
     */
    public String getPripravenyPrikaz() {
        return pripravenyPrikaz;
    }

    /**
     * @param pripravenyPrikaz the pripravenyPrikaz to set
     */
    public void setPripravenyPrikaz(String pripravenyPrikaz) {
        this.pripravenyPrikaz = pripravenyPrikaz;
    }

    /**
     * @return the tabulky
     */
    public ArrayList<tabulka_stlpce> getTabulky() {
        return tabulky;
    }

    /**
     * @param tabulky the tabulky to set
     */
    public void setTabulky(ArrayList<tabulka_stlpce> tabulky) {
        this.tabulky = tabulky;
    }

    /**
     * @return the doplnkove
     */
    public ArrayList<String> getDoplnkove() {
        return doplnkove;
    }

    /**
     * @param doplnkove the doplnkove to set
     */
    public void setDoplnkove(ArrayList<String> doplnkove) {
        this.doplnkove = doplnkove;
    }

    /**
     * @return the zobrazit
     */
    public boolean isZobrazit() {
        return zobrazit;
    }

    /**
     * @param zobrazit the zobrazit to set
     */
    public void setZobrazit(boolean zobrazit) {
        this.zobrazit = zobrazit;
    }

    /**
     * @return the z_jednej_tabulky
     */
    public boolean isZ_jednej_tabulky() {
        return z_jednej_tabulky;
    }

    /**
     * @param z_jednej_tabulky the z_jednej_tabulky to set
     */
    public void setZ_jednej_tabulky(boolean z_jednej_tabulky) {
        this.z_jednej_tabulky = z_jednej_tabulky;
    }

    /**
     * @return the pk
     */
    public int getPk() {
        return pk;
    }

    /**
     * @param pk the pk to set
     */
    public void setPk(int pk) {
        this.pk = pk;
    }

    /**
     * @return the group_by
     */
    public String getGroup_by() {
        return group_by;
    }

    /**
     * @param group_by the group_by to set
     */
    public void setGroup_by(String group_by) {
        this.group_by = group_by;
    }

    /**
     * @return the cela_tabulka
     */
    public boolean isCela_tabulka() {
        return cela_tabulka;
    }

    /**
     * @param cela_tabulka the cela_tabulka to set
     */
    public void setCela_tabulka(boolean cela_tabulka) {
        this.cela_tabulka = cela_tabulka;
    }

    /**
     * @return the join
     */
    public ArrayList<String> getJoin() {
        return join;
    }

    /**
     * @param join the join to set
     */
    public void setJoin(ArrayList<String> join) {
        this.join = join;
    }

    /**
     * @return the on_1
     */
    public ArrayList<String> getOn_1() {
        return on_1;
    }

    /**
     * @param on_1 the on_1 to set
     */
    public void setOn_1(ArrayList<String> on_1) {
        this.on_1 = on_1;
    }

    /**
     * @return the on_2
     */
    public ArrayList<String> getOn_2() {
        return on_2;
    }

    /**
     * @param on_2 the on_2 to set
     */
    public void setOn_2(ArrayList<String> on_2) {
        this.on_2 = on_2;
    }

    /**
     * @return the join2
     */
    public Object getJoin2() {
        return join2;
    }

    /**
     * @param join2 the join2 to set
     */
    public void setJoin2(Object join2) {
        this.setJoin2(join2);
    }

    /**
     * @param join2 the join2 to set
     */
    public void setJoin2(ArrayList<Object> join2) {
        this.join2 = join2;
    }

  

    
    
}
