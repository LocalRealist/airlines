/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

import java.util.ArrayList;

/**
 *
 * @author aaa
 */
public class odpoved_selectu 
{
    public ArrayList<Object> objekty = new ArrayList<>();
    public boolean zobrazit;
    public boolean prazdne;
    public boolean z_jednej_tabulky;
    public ArrayList<String> stlpce;
    public ArrayList<String> nazvy_stlpcov;
    public ArrayList<ArrayList<String>> obsah;
    public ArrayList<String> extra_stlpce;
    public ArrayList<ArrayList<String>> extra_obsah;
    
    public odpoved_selectu()
    {
        objekty = new ArrayList<>();
        zobrazit = false;
        prazdne = true;
        stlpce = new ArrayList<>();
        z_jednej_tabulky = false;
        obsah = new ArrayList();
        nazvy_stlpcov = new ArrayList<>();
        extra_stlpce = new ArrayList<>();
        extra_obsah = new ArrayList<>();
    }

    /**
     * @return the objekty
     */
    public ArrayList<Object> getObjekty() {
        return objekty;
    }

    /**
     * @param objekty the objekty to set
     */
    public void setObjekty(ArrayList<Object> objekty) {
        this.objekty = objekty;
    }

    /**
     * @return the zobrazit
     */
    public boolean isZobrazit() {
        return zobrazit;
    }

    /**
     * @param zobrazit the zobrazit to set
     */
    public void setZobrazit(boolean zobrazit) {
        this.zobrazit = zobrazit;
    }

    /**
     * @return the prazdne
     */
    public boolean isPrazdne() {
        return prazdne;
    }

    /**
     * @param prazdne the prazdne to set
     */
    public void setPrazdne(boolean prazdne) {
        this.prazdne = prazdne;
    }

    /**
     * @return the stlpce
     */
    public ArrayList<String> getStlpce() {
        return stlpce;
    }

    /**
     * @param stlpce the stlpce to set
     */
    public void setStlpce(ArrayList<String> stlpce) {
        this.stlpce = stlpce;
    }

    /**
     * @return the z_jednej_tabulky
     */
    public boolean isZ_jednej_tabulky() {
        return z_jednej_tabulky;
    }

    /**
     * @param z_jednej_tabulky the z_jednej_tabulky to set
     */
    public void setZ_jednej_tabulky(boolean z_jednej_tabulky) {
        this.z_jednej_tabulky = z_jednej_tabulky;
    }

    /**
     * @return the nazvy_stlpcov
     */
    public ArrayList<String> getNazvy_stlpcov() {
        return nazvy_stlpcov;
    }

    /**
     * @param nazvy_stlpcov the nazvy_stlpcov to set
     */
    public void setNazvy_stlpcov(ArrayList<String> nazvy_stlpcov) {
        this.nazvy_stlpcov = nazvy_stlpcov;
    }

    /**
     * @return the obsah
     */
    public ArrayList<ArrayList<String>> getObsah() {
        return obsah;
    }

    /**
     * @param obsah the obsah to set
     */
    public void setObsah(ArrayList<ArrayList<String>> obsah) {
        this.obsah = obsah;
    }
    
}
